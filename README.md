![ReactJs](https://user-images.githubusercontent.com/56060354/97405855-53c40280-191e-11eb-8fe5-8d7878b0b280.png)

![Author](https://img.shields.io/badge/author-Brijesh%20Burfal-lightgrey.svg?colorB=9900cc&style=flat-square)
# ConnectIN
ConnectIN (ConIN) helps people to socially connect to a place and share there opinion

It is a Full Stack (MERN) application that has multiple features .

## Components
 - ConnIN-api:  I have created an API called ConnIN-api using MERN stack and it handles all the routes and CRUD requests
 - COnnIN-ui:   It  has the front end code of our application majorly uses React and Bootstrap , material -ui.

## FEATURES
- Follow/Unfollow
- Add Picture, Post etc to your Feed
- View Profile
### SIGNUP
![image](https://user-images.githubusercontent.com/56060354/126996616-c9dcb000-5ab8-4f64-93d4-e858e1e20e3f.png)
### LOGIN
![image](https://user-images.githubusercontent.com/56060354/126996829-2e6588bc-0d1c-4883-bd8b-d3338384d7ac.png)
### FEED VIEW
![image](https://user-images.githubusercontent.com/56060354/126997070-2e46b290-d70e-44bf-be8f-3e411036ea81.png)
### VIEW & ADD POSTS TO TIMELINE  
![image](https://user-images.githubusercontent.com/56060354/126997425-b07c45aa-2e3f-4155-85a2-f66fc59633af.png)
### VIEW YOUR PROFILE
![image](https://user-images.githubusercontent.com/56060354/126997806-841f4377-b414-48a0-99bc-09f930229b18.png)
### VIEW YOUR FRIENDS PROFILE
![image](https://user-images.githubusercontent.com/56060354/126998752-879c7fcf-5e2d-437b-8fb9-61731d2f4ca2.png)
### TECH STACK
- MERN STACK
  - M = MongoDB (Mongoose for Node.js to work with MongoDB &  MongoDB Atlas = MongoDB database at cloud).
  - E = Express (Nodejs, cors middleware for access other servers outside our server).
  - R = React (for the frontend , bootstrap for styling, react-router-dom for React routes, and axios to connect to the backend).
  - N = Node.js (Server , dotenv, nodemon ).

### BACKEND DATABASE AT MONGO DB ATLAS
![image](https://user-images.githubusercontent.com/56060354/126998023-1c1290d5-b590-411a-b9ea-7e5db71663d0.png)

